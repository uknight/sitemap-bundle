# U-Knight Sitemap Bundle

Simple configurable bundle for generating simple sitemap.xml

## Instruction

### Prerequisites

You need:

1. symfony
2. doctrine
3. twig
4. brain and straight arms
5. this installation instructions

### Installation

Add to your ```composer.json``` file following lines:

```json
    ...
    "repositories": [
        {
            "url": "git@bitbucket.org:uknight/sitemap-bundle.git",
            "type": "git"
        }
    ]
    ...
```

```json
    "require": {
        ...
        "uknight/sitemap-bundle": "@dev"
    },
```

Then run

```bash
$ composer install
```

### Configuration

Add following lines to your ```config/bundles.php``` file:

```php
return [
    ...
    Uknight\SitemapBundle\UknightSitemapBundle::class => ['all' => true],
];
```

Add ```config/bundles/uknight_sitemap.yaml``` with such a content:

```yaml
uknight_sitemap:
    languages:                         # Array of language codes for languages to use
        - langcode                     # language code
    routes:                            # Required Array of your routes
        -
            name:                 ~    # Required Route name
            priority:             0.5  # Sitemap priority
            entity:               null # Full entity name, e.g.: Acme\FooBundle\Entity\BarEntity
            parameters:                # Array of url parameters
                -
                    url_field:    slug # Path parameter name
                    entity_field: slug # Entity field to use as value
                    child_position: null # If entity field is collection
                    child_field:  null # If entity field is collection, it's child field to use
            lastmod_field:        null # Datetime field to use
            filter_field:         null # Entity field to filter by
            filter_value:         null # Value of this field to filter by
            change_frequency:     null # One of the following: always|hourly|daily|weekly|monthly|yearly|never
```

Now you can just visit ```http://yoursite.com/sitemap.xml```, that's it! Really!


## Credits

Big thanks to myself, Flash from U-Knight Web Studio LLC [u-knight.de](https://u-knight.de/en/) , for making this bundle ;)