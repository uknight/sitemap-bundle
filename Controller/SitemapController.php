<?php

namespace Uknight\SitemapBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/sitemap.xml", name="sitemap")
     */
    #[Route("/sitemap.xml", name: "sitemap")]
    public function sitemap(EntityManagerInterface $em, ParameterBagInterface $param)
    {
        $routes = $param->get('uknight_sitemap.routes');
        $languages = $param->get('uknight_sitemap.languages');

        foreach ($routes as $key => $route) {

            if ($route['filter_field'] !== null && $route['filter_value'] !== null) {
                $filter = [$route['filter_field'] => $route['filter_value']];
            } else {
                $filter = [];
            }

            if ($route['entity'] !== null) {
                $routes[$key]['items'] = $em
                    ->getRepository($route['entity'])
                    ->findBy($filter);
            }
        }

        return new Response(
            $this->renderView('@UknightSitemap/Sitemap/sitemap.xml.twig', [
                'routes'    => $routes,
                'languages' => $languages,
            ]),
            200,
            [
                'Content'      => 'text/xml',
                'Content-Type' => 'text/xml'
            ]
        );
    }
}
