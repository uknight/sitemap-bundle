<?php
/**
 * Created by PhpStorm.
 * User: flash
 * Date: 10.09.18
 * Time: 16:40
 */

namespace Uknight\SitemapBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class UknightSitemapExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $container->setParameter('uknight_sitemap.routes', $config['routes']);
        $container->setParameter('uknight_sitemap.languages', $config['languages']);
    }
}