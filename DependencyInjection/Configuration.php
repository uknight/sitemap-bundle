<?php
/**
 * Created by PhpStorm.
 * User: flash
 * Date: 10.09.18
 * Time: 16:24
 */

namespace Uknight\SitemapBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('uknight_sitemap');

        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('languages')
                    ->scalarPrototype()->end()
                ->end()
                ->arrayNode('routes')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('name')->isRequired()->end()
                            ->floatNode('priority')->defaultValue(0.5)->end()
                            ->scalarNode('entity')->defaultNull()->end()
                            ->arrayNode('parameters')
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('url_field')->defaultValue('slug')->end()
                                        ->scalarNode('entity_field')->defaultValue('slug')->end()
                                        ->integerNode('child_position')->defaultNull()->end()
                                        ->scalarNode('child_field')->defaultNull()->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->scalarNode('lastmod_field')->defaultNull()->end()
                            ->scalarNode('filter_field')->defaultNull()->end()
                            ->scalarNode('filter_value')->defaultNull()->end()
                            ->enumNode('change_frequency')
                                ->values(['always', 'hourly', 'daily', 'weekly', 'monthly', 'yearly', 'never'])
                                ->defaultNull()
                            ->end()
                        ->end()
                    ->end()
                ->end() // twitter
            ->end()
        ;

        return $treeBuilder;
    }
}
